define([
	'html!ui/templates/trade/tplItem',
	'html!ui/templates/trade/tplAmount',
	'html!ui/templates/trade/template',
	'css!ui/templates/trade/styles',
	'js/system/events'
], function (
	tplItem,
	tplAmount,
	template,
	styles,
	events
) {
	const coinSrc = 'images/fish/coin.png';

	return {
		tpl: template,

		hidden: true,
		modal: true,

		coinsYou: 0,
		coinsTrader: 0,

		postRender: function () {
			this.find('.button').on('click', this.trade.bind(this));
		},

		trade: function () {
			const uiPockets = $('.uiPockets').data('ui');
			const uiMerchant = $('.uiMerchant').data('ui');

			this.find('.give').children().toArray().forEach(c => {
				const isCoin = $(c).hasClass('coin');
				const item = $(c).data('item');

				if (isCoin) {
					uiMerchant.getCoins(item.worth);
					uiPockets.getCoins(-item.worth);
					return;
				}

				uiPockets.items.spliceWhere(i => i === item);
				uiMerchant.items.push(item);
			});

			this.find('.get').children().toArray().forEach(c => {
				const isCoin = $(c).hasClass('coin');
				const item = $(c).data('item');

				if (isCoin) {
					uiPockets.getCoins(item.worth);
					uiMerchant.getCoins(-item.worth);
					return;
				}

				uiMerchant.items.spliceWhere(i => i === item);
				uiPockets.items.push(item);
			});

			this.hide();
		},

		onAfterShow: function () {
			this.find('.items').empty();

			const uiPockets = $('.uiPockets').data('ui');
			this.coinsYou = uiPockets.coins;
			uiPockets.items.forEach(i => {
				this.buildItem('.row.you .items.have', i, '.swap.give', true);
			});

			this.buildItem('.row.you .items.have', { src: coinSrc, worth: uiPockets.coins }, null, true).addClass('coin');

			const uiMerchant = $('.uiMerchant').data('ui');
			this.coinsTrader = uiMerchant.coins;
			uiMerchant.items.forEach(i => {
				const canAfford = uiPockets.canAfford(i.worth);
				this.buildItem('.row.trader .items.have', i, '.swap.get', canAfford);
			});

			this.buildItem('.row.trader .items.have', { src: coinSrc, worth: uiMerchant.coins }, null, true).addClass('coin');
		},

		buildItem: function (selector, item, target, canAfford) {
			const { src, worth } = item;
			
			const el = $(tplItem)
				.appendTo(this.find(selector))
				.css({
					'background-image': 'url(' + src + ')'
				})
				.attr('worth', worth)
				.data('item', item);

			$(tplAmount)
				.appendTo(el)
				.html(worth);

			if (!canAfford)
				el.addClass('nope');
				
			if (target && canAfford) 
				el.on('click', this.onClickItem.bind(this, el, target));

			return el;
		},

		onClickItem: function (el, target) {
			let newTarget = el.parent().parent();
			if ($(target).hasClass('swap'))
				newTarget = newTarget.find('.have');
			else
				newTarget = newTarget.find('.swap');

			el
				.appendTo($(target))
				.off()
				.on('click', this.onClickItem.bind(this, el, newTarget));

			$(target).find('.coin').appendTo($(target));

			this.calcCoins();
			this.calcCanAfford();
		},

		calcCanAfford: function () {
			const coinsYou = (
				~~$('.you .have .coin .amount').html() +
				~~$('.trader .swap .coin .amount').html()
			);
			const coinsTrader = (
				~~$('.trader .have .coin .amount').html() +
				~~$('.you .swap .coin .amount').html()
			);

			$('.trader .have .item').toArray().forEach(i => {
				const el = $(i);
				el.removeClass('nope');
				if (~~el.attr('worth') > coinsYou)
					el.addClass('nope');
			});

			$('.you .have .item').toArray().forEach(i => {
				const el = $(i);
				el.removeClass('nope');
				if (~~el.attr('worth') > coinsTrader)
					el.addClass('nope');
			});
		},

		calcCoins: function () {
			$('.swap .coin').remove();

			const src = 'images/fish/coin.png';
			
			let give = $('.trader .swap .item').toArray().reduce((p, n) => p + ~~$(n).attr('worth'), 0);
			let get = $('.you .swap .item').toArray().reduce((p, n) => p + ~~$(n).attr('worth'), 0);

			if (get > give) {
				get -= give;
				give = 0;
			} else {
				give -= get;
				get = 0;
			}

			const elTraderCoins = $('.trader .have .coin');
			const coinsTraderLeft = this.coinsTrader - get;
			elTraderCoins.find('.amount').html(coinsTraderLeft);
			if (coinsTraderLeft === 0)
				elTraderCoins.hide();
			else
				elTraderCoins.show();

			const elYouCoins = $('.you .have .coin');
			const coinsYouLeft = this.coinsYou - give;
			elYouCoins.find('.amount').html(coinsYouLeft);
			if (coinsYouLeft === 0)
				elYouCoins.hide();
			else
				elYouCoins.show();

			if (give) 
				this.buildItem('.row.you .swap', { src, worth: give }, null, true).addClass('coin');
			
			if (get)
				this.buildItem('.row.trader .swap', { src, worth: get }, null, true).addClass('coin');
		}
	};
});
