define([
	'html!ui/templates/loot/tplLoot',
	'html!ui/templates/loot/template',
	'css!ui/templates/loot/styles',
	'js/system/events',
	'js/sound/sound'
], function (
	tplLoot,
	template,
	styles,
	events,
	sound
) {
	const maxClicks = 3;

	return {
		tpl: template,

		hidden: true,

		clicksLeft: 0,

		postRender: function () {
			
		},

		setLoot: function ({ src, cb }) {
			this.clicksLeft = maxClicks;

			$(tplLoot)
				.appendTo(this.find('.bottom'))
				.on('click', this.onClickLoot.bind(this))
				.on('animationend', this.onAnimationEnd.bind(this));

			this.show();
		},

		onClickLoot: function () {
			sound.play('rustle');
			this.find('.loot').addClass('shaking');
			this.clicksLeft--;
			if (!this.clicksLeft) {
				events.emit('onLootOpen');
				this.hide();
			}
		},

		onAnimationEnd: function () {
			this.find('.loot').removeClass('shaking');
		}
	};
});
