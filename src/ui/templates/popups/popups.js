define([
	'html!ui/templates/popups/template',
	'css!ui/templates/popups/styles',
	'html!ui/templates/popups/tplPopup'
], function (
	template,
	styles,
	tplPopup
) {
	return {
		tpl: template,

		postRender: function () {
			this.onEvent('onGetItem', this.onGetItem.bind(this));
		},

		onGetItem: function () {
			const pockets = $('.uiPockets').data('ui').items;
			const { src } = pockets[pockets.length - 1];

			const el = $(tplPopup)
				.appendTo(this.el)
				.find('.img')
				.css({
					background: 'url(' + src + ')'
				})
				.parent();

			setTimeout(() => {
				el
					.addClass('fading')
					.on('animationend', () => el.remove());
			}, 1500);
		}
	};
});
