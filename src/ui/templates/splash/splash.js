define([
	'html!ui/templates/splash/template',
	'css!ui/templates/splash/styles',
	'js/sound/sound'
], function (
	template,
	styles,
	sound
) {
	return {
		tpl: template,

		centered: true,

		postRender: function () {
			
		},

		unsplash: async function () {
			return new Promise(res => {
				this.el.addClass('vanish');
				this.el.on('animationend', () => {
					sound.activate();
					this.destroy();
					res();
				});
			});
		}
	};
});
