define([
	'ui/factory',
	'js/system/events'
], (
	uiFactory,
	events
) => {
	return {
		checkPockets: {
			cpt: 'Check Pockets',
			cb: () => {
				$('.uiPockets').data('ui').toggle();
			}
		},

		catchFish: {
			cpt: 'Catch Fish',
			cb: () => {
				events.emit('onGetItem');
			}
		},

		trade: {
			cpt: 'Trade',
			cb: () => {
				$('.uiTrade').data('ui').toggle();
			}
		}
	};
});
