define([
	'html!ui/templates/actions/tplButton',
	'html!ui/templates/actions/template',
	'css!ui/templates/actions/styles',
	'ui/templates/actions/config',
	'js/sound/sound'
], function (
	tplButton,
	template,
	styles,
	config,
	sound
) {
	return {
		tpl: template,

		postRender: function () {
			
		},

		clearButtons: function () {
			this.find('.button').remove();
		},

		addButton: function ({ cpt, cb, clear, tpl }) {
			if (tpl) {
				const loaded = config[tpl];
				cpt = loaded.cpt;
				cb = loaded.cb;
			}

			if (clear)
				this.clearButtons();

			$(tplButton)
				.appendTo(this.el)
				.on('mousedown', () => {
					if (cb)
						cb();
					sound.play('click');
				})
				.find('.cpt')
				.html(cpt);
		}
	};
});
