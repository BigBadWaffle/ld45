define([
	'html!ui/templates/pockets/tplItem',
	'html!ui/templates/pockets/template',
	'css!ui/templates/pockets/styles',
	'js/system/events'
], function (
	tplItem,
	template,
	styles,
	events
) {
	return {
		tpl: template,

		hidden: true,
		modal: true,

		items: [],
		coins: 100,

		postRender: function () {
			this.buildItem('rod1', undefined, 'rod');

			this.onEvent('onGetItem', this.onGetItem.bind(this));
		},

		onGetItem: function (item) {
			this.buildItem(['fish1', 'fish2', 'fish3', 'fish4', 'fish5', 'fish6'][~~(Math.random() * 6)]);
		},

		buildItem: function (img, folder = 'fish', type = 'fish') {
			const name = img;
			const src = 'images/' + folder + '/' + img + '.png';

			this.items.push({
				name,
				src,
				worth: 1 + ~~(Math.random() * 99),
				type
			});
		},

		onAfterShow: function () {
			this.find('.bottom').empty();

			this.items.forEach(({ src }) => {
				$(tplItem)
					.appendTo(this.find('.bottom'))
					.css({
						'background-image': 'url(' + src + ')'
					});
			});
		},

		canAfford: function (amount) {
			return amount <= this.coins;
		},

		getCoins: function (amount) {
			this.coins += amount;
		}
	};
});
