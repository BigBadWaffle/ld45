define([
	'html!ui/templates/rods/template',
	'css!ui/templates/rods/styles',
	'html!ui/templates/rods/tplRod',
	'js/system/events'
], function (
	template,
	styles,
	tplRod,
	events
) {
	const { stateAiming, stateMeasuring } = {
		stateAiming: 1,
		stateMeasuring: 2
	};

	const minAngle = -90;
	const maxAngle = 90;
	const angleSpeed = 2;

	const minScale = 0.5;
	const maxScale = 1.5;
	const scaleSpeed = 0.03;

	return {
		tpl: template,

		centered: true,
		state: null,
		activeRod: null,
		angle: 0,
		angleDir: 1,
		scale: 1,
		scaleDir: 1,

		postRender: function () {
			$('.uiPockets').data('ui')
				.items
				.filter(i => i.type === 'rod')
				.forEach(({ src }) => {
					const el = $(tplRod)
						.appendTo(this.el)
						.find('.img')
						.css({
							background: 'url(' + src + ')'
						})
						.parent();

					el
						.on('mousedown', this.mouseDown.bind(this, el))
						.on('mouseup', this.mouseUp.bind(this, el));
				});
		},

		mouseDown: function (el) {
			if (!this.state) {
				el.addClass('active');
				this.startAiming(el);
			} else if (this.state === stateMeasuring) {
				el.removeClass('active');
				this.catch();
			}
		},

		mouseUp: function (el) {
			if (this.state === stateAiming)
				this.startMeasuring(el);
		},

		startAiming: function (el) {
			this.angle = 0;
			this.angleDir = 1;
			this.state = stateAiming;
			this.activeRod = el;

			this.updateAim();
		},

		startMeasuring: function (el) {
			this.scale = 1;
			this.scaleDir = 1;
			this.state = stateMeasuring;

			this.updateMeasure();
		},

		catch: function () {
			this.activeRod = null;
			this.state = null;
			events.emit('onGetItem');
		},

		updateAim: function () {
			if (this.state !== stateAiming)
				return;

			this.angle += this.angleDir * angleSpeed;
			this.activeRod.find('.arrow').css('transform', 'rotate(' + this.angle + 'deg)');
			
			if (
				(this.angleDir === 1 && this.angle >= maxAngle) ||
				(this.angleDir === -1 && this.angle <= minAngle)
			)
				this.angleDir *= -1;

			requestAnimationFrame(this.updateAim.bind(this));
		},

		updateMeasure: function () {
			if (this.state !== stateMeasuring)
				return;

			this.scale += this.scaleDir * scaleSpeed;
			this.activeRod.find('.arrow').css('transform', 'rotate(' + this.angle + 'deg) scaleY(' + this.scale + ')');
			
			if (
				(this.scaleDir === 1 && this.scale >= maxScale) ||
				(this.scaleDir === -1 && this.scale <= minScale)
			)
				this.scaleDir *= -1;

			requestAnimationFrame(this.updateMeasure.bind(this));
		}
	};
});
