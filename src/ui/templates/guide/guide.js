define([
	'html!ui/templates/guide/template',
	'css!ui/templates/guide/styles',
	'js/misc/transformText'
], function (
	template,
	styles,
	transformText
) {
	return {
		tpl: template,

		msg: '',
		subtext: '',

		postRender: function () {
			this.updateMsg();
		},

		updateMsg: async function () {
			const elSubtext = this.find('.subtext').addClass('hidden');

			await transformText(this.find('.msg'), this.msg);
			this.find('.subtext').html(this.subtext).removeClass('hidden');
		}
	};
});
