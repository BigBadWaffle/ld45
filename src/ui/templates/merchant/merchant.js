define([
	'html!ui/templates/merchant/template',
	'css!ui/templates/merchant/styles'
], function (
	template,
	styles
) {
	return {
		tpl: template,

		msg: '',
		subtext: '',

		items: [],
		coins: 10000,

		postRender: function () {
			this.updateMsg();

			for (let i = 0; i < 9; i++) 
				this.buildItem();
		},

		buildItem: function () {
			const name = ['rod2', 'rod3', 'hook1', 'hook2', 'hook3'][~~(Math.random() * 5)];
			const src = 'images/fish/' + name + '.png';

			this.items.push({
				name,
				src,
				worth: 1 + ~~(Math.random() * 99)
			});
		},

		updateMsg: function () {
			this.find('.msg').html(this.msg);
		},

		canAfford: function (amount) {
			return amount <= this.coins;
		},

		getCoins: function (amount) {
			this.coins += amount;
		}
	};
});
