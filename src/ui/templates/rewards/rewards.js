define([
	'html!ui/templates/rewards/tplReward',
	'html!ui/templates/rewards/template',
	'css!ui/templates/rewards/styles',
	'js/system/events'
], function (
	tplReward,
	template,
	styles,
	events
) {
	return {
		tpl: template,

		postRender: function () {
			
		},

		setRewards: function ({ src, cb }) {
			for (let i = 0; i < 1; i++) {
				$(tplReward)
					.appendTo(this.find('.bottom'))
					.on('click', this.onClickReward.bind(this));
			}
		},

		onClickReward: function () {
			events.emit('onRewardsGetAll');
			this.hide();
		}
	};
});
