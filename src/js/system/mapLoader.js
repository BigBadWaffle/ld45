define([
	'js/misc/physics'
], function (
	physics
) {
	return {
		w: 0,
		h: 0,

		layers: null,
		tilesets: null,

		cbReady: null,

		load: async function (name) {
			return new Promise(res => {
				this.cbReady = res;

				require(['json!maps/' + name + '.json'], this.onLoadMap.bind(this));
			});			
		},

		onLoadMap: function (map) {
			this.w = map.width;
			this.h = map.height;

			let collisionMap = _.get2dArray(this.w, this.h, 0);

			this.tilesets = map.tilesets
				.map(t => {
					return {
						name: t.name,
						first: t.firstgid,
						count: t.tilecount
					};
				});

			this.layers = map.layers
				.map(l => {
					let res = {
						name: l.name
					};

					if (l.data) {
						res.data = _.get2dArray(this.w, this.h);

						l.data.forEach((d, i) => {
							const y = ~~(i / this.w);
							const x = i - (y * this.w);

							let mapped = d ? this.mapTile(d) : null;
							res.data[x][y] = mapped;

							if (mapped !== null && l.name === 'walls')
								collisionMap[x][y] = 1;
						});
					} else if (l.objects) {
						res.objects = l.objects
							.map(r => {
								let res = {
									name: r.name,
									sheetName: l.name,
									x: r.x / 8,
									y: (r.y / 8) - 1,
									visible: r.visible,
									cell: this.mapTile(r.gid),
									components: Object.entries(r.properties || {})
										.map(([ k, v ]) => {
											const config = JSON.parse(v);
											return {
												type: k
													.replace('cpn', '')
													.replace(/./, k[3].toLowerCase()),
												...config
											};
										})
								};

								return res;
							});
					}

					return res;
				});

			physics.init(collisionMap);

			this.cbReady();
		},

		mapTile: function (tile) {
			const set = this.tilesets.find(t => {
				return (tile < t.first + t.count);
			});

			return tile - set.first;
		}
	};
});
