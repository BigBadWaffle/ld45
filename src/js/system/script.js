define([
	'ui/factory',
	'js/system/events',
	'js/rendering/renderer',
	'js/sound/sound'
], 
(
	uiFactory, 
	events, 
	renderer,
	sound
) => {
	const script = [
		{
			type: 'spawnUi',
			options: {
				type: 'splash'
			}
		},
		{
			wait: {
				element: '.uiSplash',
				event: 'click'
			}
		},
		{
			type: 'updateUi',
			options: {
				type: 'splash',
				callMethod: 'unsplash'
			}
		},
		{
			type: 'spawnUi',
			options: {
				type: 'guide',
				msg: 'Oh, hi there!',
				subtext: 'click to continue...',
				container: '.faces'
			},
			wait: {
				event: 'mouseDown'
			}
		},
		{
			type: 'updateUi',
			options: {
				type: 'guide',
				msg: 'There doesn\'t seem to be much around here, does there?',
				callMethod: 'updateMsg'
			},
			wait: {
				event: 'mouseDown'
			}
		},
		[
			{
				type: 'updateUi',
				options: {
					type: 'guide',
					msg: 'Maybe you should check your pockets?',
					subtext: '',
					callMethod: 'updateMsg'
				}
			},
			{
				type: 'spawnUi',
				options: {
					type: 'actions'
				}
			},
			{
				type: 'updateUi',
				options: {
					type: 'actions',
					callMethod: 'addButton',
					cpt: 'Check Pockets'
				},
				wait: {
					element: '.uiActions .button',
					event: 'mousedown',
					skipSound: true
				}
			}
		],
		{
			type: 'updateUi',
			options: {
				type: 'actions',
				callMethod: 'clearButtons'
			}
		},
		{
			type: 'updateUi',
			options: {
				type: 'guide',
				msg: 'Huh...they\'re empty.',
				subtext: 'click to continue...',
				callMethod: 'updateMsg'
			},
			wait: {
				event: 'mouseDown'
			}
		},
		[
			{
				type: 'updateUi',
				options: {
					type: 'guide',
					msg: 'Tell you what; let\'s go for a walk. Maybe we\'ll find something to do?',
					subtext: '',
					callMethod: 'updateMsg'
				},
				wait: {
					event: 'mouseDown'
				}
			},
			{
				type: 'updateUi',
				options: {
					type: 'actions',
					callMethod: 'addButton',
					cpt: 'Take a Walk'
				},
				wait: {
					element: '.uiActions .button',
					event: 'mousedown',
					skipSound: true
				}
			}
		],
		{
			type: 'updateUi',
			options: {
				type: 'actions',
				callMethod: 'clearButtons'
			}
		},
		{
			type: 'buildMap'
		},
		{
			type: 'updateUi',
			options: {
				type: 'guide',
				msg: 'What a lovely pond! Now what do you propose we do?',
				subtext: '',
				callMethod: 'updateMsg'
			}
		},
		{
			type: 'updateUi',
			options: {
				type: 'actions',
				callMethod: 'addButton',
				cpt: 'Look Around'
			},
			wait: {
				element: '.uiActions .button',
				event: 'mousedown',
				skipSound: true
			}
		},
		{
			type: 'spawnUi',
			options: {
				type: 'loot'
			}
		},
		{
			type: 'updateUi',
			options: {
				type: 'guide',
				msg: 'Ooh, a sparkly shrub. What\'s inside?',
				subtext: 'click on the shrub...',
				callMethod: 'updateMsg'
			}
		},
		{
			type: 'updateUi',
			options: {
				type: 'loot',
				callMethod: 'setLoot'
			}
		},
		{
			type: 'updateUi',
			options: {
				type: 'actions',
				callMethod: 'clearButtons'
			},
			wait: {
				element: '.uiLoot .loot',
				event: 'mousedown',
				skipSound: true
			}
		},
		{
			type: 'updateUi',
			options: {
				type: 'guide',
				msg: 'I can almost see it!',
				subtext: 'keep on clicking...',
				callMethod: 'updateMsg'
			}
		},
		{
			wait: {
				event: 'onLootOpen'
			}
		},
		{
			type: 'updateUi',
			options: {
				type: 'loot',
				callMethod: 'hide'
			}
		},
		{
			type: 'updateUi',
			options: {
				type: 'guide',
				msg: 'A fishing rod? How odd!',
				subtext: 'take the fishing rod...',
				callMethod: 'updateMsg'
			}
		},
		{
			type: 'spawnUi',
			options: {
				type: 'rewards'
			}
		},
		{
			type: 'updateUi',
			options: {
				type: 'rewards',
				callMethod: 'setRewards'
			}
		},
		{
			wait: {
				event: 'onRewardsGetAll'
			}
		},
		{
			type: 'updateUi',
			options: {
				type: 'rewards',
				callMethod: 'hide'
			}
		},
		{
			type: 'updateUi',
			options: {
				type: 'guide',
				msg: 'Good job. Now what are you waiting for? Catch some fish!',
				subtext: '',
				callMethod: 'updateMsg'
			}
		},
		{
			type: 'spawnUi',
			options: {
				type: 'pockets'
			}
		},
		{
			type: 'spawnUi',
			options: {
				type: 'popups'
			}
		},
		{
			type: 'spawnUi',
			options: {
				type: 'rods'
			}
		},
		{
			type: 'updateUi',
			options: {
				type: 'actions',
				callMethod: 'addButton',
				tpl: 'checkPockets',
				clear: true
			}
		},
		{
			wait: {
				event: 'onGetItem'
			}
		},
		{
			type: 'updateUi',
			options: {
				type: 'guide',
				callMethod: 'hide'
			}
		}/*,
		{
			type: 'spawnUi',
			options: {
				type: 'merchant',
				msg: 'Care to trade with an old merchant?',
				container: '.faces'
			}
		},
		{
			type: 'updateUi',
			options: {
				type: 'actions',
				callMethod: 'addButton',
				tpl: 'trade'
			}
		},
		{
			type: 'spawnUi',
			options: {
				type: 'trade'
			}
		}*/
	];

	const spawnUi = async options => {
		await uiFactory.build(options.type, options);
	};

	const updateUi = async ({ type, callMethod, ...rest }) => {
		const ui = $(`.ui${type.replace(/./, type[0].toUpperCase())}`).data('ui');
		$.extend(ui, rest);
		
		if (callMethod)
			await ui[callMethod](rest);
	};

	const buildMap = () => {
		renderer.buildMap();
	};

	return {
		nextStep: 0,

		play: async function () {
			let step = script[this.nextStep++];
			if (!step)
				return;

			if (!step.push)
				step = [ step ];

			let foundWait = null;

			for (const s of step) {
				const { type, options, wait } = s;
				if (type === 'spawnUi')
					await spawnUi(options);
				else if (type === 'updateUi')
					await updateUi(options);
				else if (type === 'buildMap')
					buildMap();

				if (wait)
					foundWait = wait;
			}

			if (foundWait) {
				if (!dev)
					await this.doWait(foundWait);
				this.play();
			} else
				this.play();
		},

		doWait: async function ({ event, element, skipSound }) {
			return new Promise(res => {
				if (!element) {
					let cb = null;
					const off = () => {
						events.off(event, cb);
					};
					cb = () => {
						sound.play('click');
						off();
						res();
					};
					events.on(event, cb);
				} else {
					const el = $(element);
					const cb = () => {
						if (!skipSound)
							sound.play('click');

						el.off(event, cb);
						res();
					};

					el.on(event, cb);				
				}
			});
		}
	};
});
