define([
	'js/input'
], function (
	input
) {
	return {
		type: 'railBuilder',

		lastPos: { 
			x: -1, 
			y: -1 
		},

		building: false,

		init: function () {
			this.hookEvent('mouseDown', this.events.onBuild.bind(this, true));
			this.hookEvent('mouseUp', this.events.onBuild.bind(this, false));
		},

		setLast: function (x = -1, y = -1) {
			this.lastPos.x = x;
			this.lastPos.y = y;
		},

		update: function () {
			if (!this.building)
				return;

			const mouse = input.mouse;
			this.buildRail(mouse);
		},

		buildRail: function ({ x, y }) {
			const tx = ~~(x / scale);
			const ty = ~~(y / scale);

			const { x: lx, y: ly } = this.lastPos;
			//Haven't moved to a new tile;
			if (lx === tx && ly === ty)
				return;

			this.setLast(tx, ty);

			objects.buildObject({
				layerName: 'rails',
				sheetName: 'sprites',
				cell: 2,
				x: tx,
				y: ty
			});
		},

		events: {
			onBuild: function (building, e) {	
				this.building = building;

				if (e.button !== 0) 
					return;

				if (!building) {
					this.setLast();
					return;
				}

				this.buildRail(e);
			}
		},

		destroy: function () {
			this.unhookEvents();
		}
	};
});
