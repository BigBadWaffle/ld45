define([], () => {
	return async (el, to) => {
		return new Promise(res => {
			el = $(el);

			const old = el.html();
			let dir = old.length ? -1 : 1;
			let index = old.length ? old.length - 1 : 0;

			const transformer = () => {
				let state = old;
				if (dir === 1)
					state = to;

				state = state.substr(0, index);
				index += dir;

				el.html(state);

				if (dir === -1 && index === 0)
					dir = 1;

				if (dir === 1 && index === to.length + 1) {
					res();
					return;
				}

				if (dir === -1) {
					if (!dev)
						requestAnimationFrame(transformer);
					else
						transformer();
					return;
				}

				if (!dev) {
					const delay = (1 + ~~(Math.random() * 2));
					setTimeout(() => requestAnimationFrame(transformer), delay);
				} else
					transformer();
			};

			transformer();
		});
	};
});
