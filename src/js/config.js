define([], () => {
	return {
		resourceList: [
			{
				name: 'grassland',
				src: 'images/grassland.png'
			}
		],
		audioList: [
			'audio/bensound-cute.mp3',
			'audio/click.ogg',
			'audio/rustle.ogg'
		]
	};
});
