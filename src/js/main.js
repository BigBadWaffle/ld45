define([
	'ui/factory',
	'js/rendering/renderer',
	'js/objects/objects',
	'js/input',
	'js/resources',
	'js/system/mapLoader',
	'js/system/script',
	'js/sound/sound',
	'js/system/events'
], function (
	uiFactory,
	renderer,
	objects,
	input,
	resources,
	mapLoader,
	script,
	sound,
	events
) {
	//Global module...naughty
	window.objects = objects;

	const modules = [objects, uiFactory, renderer];

	return {
		hasFocus: true,

		init: async function () {
			events.on('onKeyDown', key => {
				if (key === 'm')
					sound.toggleMute();
			});

			sound.init();
			await resources.init();
			await mapLoader.load('1');

			window.onfocus = this.onFocus.bind(this, true);
			window.onblur = this.onFocus.bind(this, false);

			$(window).on('contextmenu', this.onContextMenu.bind(this));

			input.init();
			modules.forEach(m => m.init());

			script.play();

			this.update();
		},

		onFocus: function (hasFocus) {
			this.hasFocus = true;
		},

		onContextMenu: function (e) {
			const allowed = [].some(s => $(e.target).hasClass(s));
			if (!allowed) {
				e.preventDefault();
				return false;
			}
		},

		update: function () {
			modules.forEach(m => m.update());

			requestAnimationFrame(this.update.bind(this));
		}
	};
});
