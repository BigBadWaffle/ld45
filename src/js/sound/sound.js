define([
	'howler'
], function (
	howler
) {
	const files = [
		{
			file: 'bensound-cute.mp3',
			doAutoplay: true,
			volume: 0.5,
			loop: true
		},
		{
			name: 'click',
			file: 'click.ogg',
			volume: 0.5
		},
		{
			name: 'rustle',
			file: 'rustle.ogg',
			volume: 0.5
		}
	];

	return {
		sounds: [],
		muted: false,

		init: function () {
			files.forEach(f => this.addSound(f));
		},

		activate: function () {
			this.sounds.forEach(s => {
				if (s.doAutoplay)
					s.play();
			});
		},

		toggleMute: function () {
			const muted = this.muted = !this.muted;

			this.sounds.forEach(s => s.mute(muted));
		},

		play: function (soundName) {
			this.sounds.find(({ name }) => name === soundName).play();
		},

		addSound: function ({ file, name, doAutoplay, ...rest }) {
			let sound = new Howl({
				src: ['audio/' + file],
				...rest
			});

			sound.name = name;
			sound.doAutoplay = doAutoplay;

			this.sounds.push(sound);
		}
	};
});
