define([
	'js/objects/componentLoader'
	//'js/rendering/renderer',
	//'js/system/events'
], function (
	componentLoader,
	//renderer,
	//events
) {
	return {
		components: [],
		eventCallbacks: {},

		addComponent: async function (options) {
			const type = options.type;
			let c = this[type];

			if ((!c) || (options.new)) {
				let template = await componentLoader.getTemplate(type);
				if (!template)
					return;

				c = $.extend(true, {}, template);
				c.obj = this;

				for (let o in options) 
					c[o] = options[o];

				if (c.init)
					c.init(options);
	
				this[c.type] = c;
				this.components.push(c);

				return c;
			} 
			if (c.extend)
				c.extend(options);

			return c;
		},

		removeComponent: function (type) {
			let cpn = this[type];
			if (!cpn)
				return;

			this.components.spliceWhere(function (c) {
				return (c === cpn);
			});

			delete this[type];
		},

		update: function () {
			let oComponents = this.components;
			let len = oComponents.length;
			for (let i = 0; i < len; i++) {
				let c = oComponents[i];
				if (c.update)
					c.update();

				if (c.destroyed) {
					if (c.destroy)
						c.destroy();

					oComponents.splice(i, 1);
					i--;
					len--;
					delete this[c.type];
				}
			}
		},

		on: function (eventName, callback) {
			let list = this.eventCallbacks[eventName] || (this.eventCallbacks[eventName] = []);
			list.push(events.on(eventName, callback));
		},

		setSpritePosition: function () {
			if (!this.sprite)
				return;

			let oldY = this.sprite.y;

			this.sprite.x = (this.x * scale) + (this.flipX ? scale : 0) + this.offsetX;
			this.sprite.y = (this.y * scale) + this.offsetY;

			if (oldY !== this.sprite.y)
				renderer.reorder();

			this.sprite.scale.x = (this.flipX ? -scaleMult : scaleMult);

			['nameSprite'].forEach(function (s, i) {
				let sprite = this[s];
				if (!sprite)
					return;

				sprite.x = (this.x * scale) + (scale / 2) - (sprite.width / 2);
				sprite.y = (this.y * scale) + yAdd;
			}, this);
		},

		setVisible: function (visible) {
			if (this.sprite)
				this.sprite.visible = visible;

			if (this.nameSprite)
				this.nameSprite.visible = ((visible) && (config.showNames));

			this.components.forEach(function (c) {
				if (c.setVisible)
					c.setVisible(visible);
			});
		},

		destroy: function () {
			if (this.sprite)
				renderer.destroyObject(this);

			if (this.nameSprite) {
				renderer.destroyObject({
					layerName: 'effects',
					sprite: this.nameSprite
				});
			}

			let oComponents = this.components;
			let cLen = oComponents.length;
			for (let i = 0; i < cLen; i++) {
				let c = oComponents[i];
				if (c.destroy)
					c.destroy();
			}

			this.destroyed = true;

			this.offEvents();
		},

		offEvents: function () {
			for (let e in this.eventCallbacks) {
				this.eventCallbacks[e].forEach(function (c) {
					events.off(e, c);
				}, this);
			}
		}
	};
});
