define([
	'js/objects/objBase',
	'js/system/events',
	'js/rendering/renderer',
	'js/sound/sound',
	'js/config'
], function (
	objBase,
	events,
	renderer,
	sound,
	config
) {
	return {
		objects: [],

		init: function () {

		},

		buildObject: async function (template) {
			let obj = $.extend(true, {}, objBase);

			let components = template.components || [];
			delete template.components;

			Object.entries(template).forEach(([k, v]) => {
				obj[k] = v;
			});

			for (const c of components) 
				await obj.addComponent(c);

			if (obj.sheetName) 
				obj.sprite = renderer.buildObject(obj);

			this.objects.push(obj);

			return obj;
		},

		update: function () {
			let objects = this.objects;
			let len = objects.length;

			for (let i = 0; i < len; i++) {
				let o = objects[i];

				if (o.destroyed) {
					o.destroy();
					objects.splice(i, 1);
					i--;
					len--;
					continue;
				}

				o.update();
			}
		}
	};
});
