let components = [
].map(function (c) {
	return 'js/components/' + c;
});

define([
	...components, 
	'../system/events'
], function () {
	const events = arguments[arguments.length - 1];
	
	const hookEvent = function (e, cb) {
		if (!this.eventList[e])
			this.eventList[e] = [];

		this.eventList[e].push(cb);
		events.on(e, cb);
	};

	const unhookEvents = function () {
		Object.entries(this.eventList).forEach(([eventName, callbacks]) => {
			callbacks.forEach(c => events.off(eventName, c));
		});
	};

	let templates = {};

	const bindComponent = c => {
		c.eventList = {};
		c.hookEvent = hookEvent.bind(c);
		c.unhookEvents = unhookEvents.bind(c);
	};

	[].forEach.call(arguments, function (t, i) {
		//Don't do this for the events module
		if (i === arguments[2].length - 1)
			return;

		bindComponent(t);

		templates[t.type] = t;
	});

	return {
		getTemplate: async function (type) {
			const template = templates[type];
			if (template) 
				return template;

			return new Promise(res => {
				require(['js/components/' + type + '.js'], c => {
					bindComponent(c);
					templates[type] = c;
					res(c);
				});
			});
		}
	};
});
