define([
	'js/config'
], function (
	{ resourceList, audioList }
) {
	let resources = {
		sprites: {},
		waiting: [],
		totalWaiting: 0,

		cbReady: null,

		init: async function () {
			this.waiting = [...resourceList.map(r => r.src), ...audioList];
			this.totalWaiting = this.waiting.length;

			return new Promise(res => {
				this.cbReady = res;

				resourceList.forEach(({ name, src }) => {
					let sprite = {
						image: new Image(),
						ready: false
					};
					sprite.image.src = src;
					sprite.image.onload = this.onSprite.bind(this, sprite, src);

					this.sprites[name] = sprite;
				});

				audioList.forEach(src => {
					const audio = $('<audio>', { src, preload: 'auto' }).appendTo('body');
					audio.on('canplaythrough', () => {
						this.waiting.spliceWhere(w => w === src);
						this.updateProgress();
					});
				});
			});
		},

		updateProgress: function () {
			const progress = ~~((1 - (this.waiting.length / this.totalWaiting)) * 100);
			$('.loader-inner').css({
				width: progress + '%'
			});

			if (!this.waiting.length) {
				$('.loader-container')
					.addClass('fading')
					.on('animationend', () => {
						$('.loader-container').remove();
						this.cbReady();
					});
			}
		},

		onSprite: function (sprite, src) {
			this.waiting.spliceWhere(w => w === src);
			sprite.ready = true;
			this.updateProgress();
		}
	};

	return resources;
});
